/*
路由器对象模块
 */

// 导入路由
import Vue from 'vue'
import VueRouter from 'vue-router'

// 引路需要使用路由的.vue文件
import MSite from '../pages/MSite/MSite'
import Order from '../pages/Order/Order'
import Profile from '../pages/Profile/Profile'
import Search from '../pages/Search/Search'
import Login from '../pages/Login/Login'

// 声明使用路由
Vue.use(VueRouter)

// 创建路由
export default new VueRouter({
  // 注册路由
  routes: [
    // 外卖
    {
      // 路由路径
      path: '/msite',
      // 映射
      component: MSite,
      // 控制当前路由所指向的页面显示  meta对象的默认值为: false (不定义的的时候是空对象)
      meta:{
        // 名字自己定义
        showFooter: true
      }
    },
    // 订单
    {
      path: '/order',
      component: Order,
      meta:{
        // 名字自己定义
        showFooter: true
      }
    },
    // 我的
    {
      path: '/profile',
      component: Profile,
      meta:{
        // 名字自己定义
        showFooter: true
      }
    },
    // 搜索
    {
      path: '/search',
      component: Search,
      meta:{
        // 名字自己定义
        showFooter: true
      }
    },
    // 登陆
    {
      path: '/login',
      component: Login
    },
    // 打开网页(默认被选中的外卖界面)
    {
      path: '/',
      redirect: '/msite'
    }
  ]
})
