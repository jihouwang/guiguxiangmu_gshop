/*
包含n个请求函数的模块
函数的返回值: promise对象
根据接口文档说明,需不需要传递参数

参数类型: 1.param  表示: ?之前
          2.query  表示: ?之后

 */

// 导入自定义的ajax模板
import ajax from './ajax.js'

// 1、根据经纬度获取位置详情
     /*
        1. export function reqAddress(geohash) {}
        2. export const req = function (geohash) {
             return ajax(`/position/${geohash}`)
           }
        参数:
            ${geohash}: 对象   param
      */
      export const reqAddress = (geohash) => ajax(`/position/${geohash}`)  // 传参数用`

// 2、获取食品分类列表
      // 不带参数
      export const reqFoodCategorys = () => ajax('/index_category')  // 不传参数'

// 3、根据经纬度获取商铺列表
      // query
      export const reqShops = (longitude, latitude) => ajax('/shops', {longitude, latitude})

// 4、根据经纬度和关键字搜索商铺列表
      // query  如果取名不同
      export  const reqSearchShop = (a,b) => ajax('/search_shops',{geohash:a,keyword:b})

// 5、获取一次性验证码
// 6、用户名密码登陆
// 7、发送短信验证码
// 8、手机号验证码登陆
// 9、根据会话获取用户信息
// 10、用户登出
