/*
ajax请求函数模块
 */
// 导入axios
import axios from 'axios'

/*
用于ajax请求  实列
参数:
  url: 请求地址
  data: 需要传入的数据源
  type: 请求类型
返回:
  Promise 表示: ajax请求返回的对象;   json格式
  (异步返回的数据是: response.data)
 */
export default function ajax (url,data={},type='GET') {
  // resolve,reject都是函数对象  resolve: 返回成功的函数;   reject: 返回失败的函数
  return new Promise(function (resolve, reject) {
    // 创建对象
    let promise
    if (type === 'GET') {
      // 准备url query  参数数据
      // 数据拼接字符串
      let dataStr = ''
      // 获取请求的数据源对象,循环对象里每个数据拼接 匿名函数
      // key: 其中的一个元素;  =>: return
      Object.keys(data).forEach((key) => {
        dataStr += key + '=' + data[key] + '&'  // key=data[key]&.....
      })
      if(dataStr!==""){
        // 从0开始,截取到最后一个&
        dataStr = dataStr.substring(0, dataStr.lastIndexOf('&'))
        url = url + '?' + dataStr   // url = www.baidu?key=data[key]&....
      }
      // 发送get请求  获取数据
      promise = axios.get(url)
    }else {
      // 发送post请求  提交数据
      promise = axios.post(url, data)
    }
    // response 返回成功的对象 想要获取数据.data
    promise.then(function (response) { // 成功返回
      // 成功了调用resolve()
      resolve(response.data)
    }).catch(function (error) { // 失败返回  error: 错误信息
      //失败了调用reject()
      reject(error)
    })
  })
}
